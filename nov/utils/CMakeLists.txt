file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)

add_library(nov_utils STATIC ${sources})

install(TARGETS nov_utils ARCHIVE DESTINATION lib)