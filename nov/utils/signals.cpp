#include "signals.hpp"

#include <cassert>
#include <csignal>

namespace Nov {
namespace Utils {

Signals* Signals::instance = nullptr;

Signals::Signals() {
	assert(instance == nullptr);
	instance = this;
}

void Signals::installSignal(int x, const Callback& c) {
	auto& info = signals[x];
	if (!info.callback) {
		info.oldCallback = std::signal(x, &staticOnSignal);
	}
	info.callback = c;
}

void Signals::installTerm(const Callback& c) {
	auto wrapper = [this, c] () {
		this->restoreSignals();
		c();
	};
	installSignal(SIGINT, wrapper);
	installSignal(SIGTERM, wrapper);
}

Signals::~Signals() {
	restoreSignals();
	instance = nullptr;
}

void Signals::restoreSignals() {
	for (const auto& x: signals) {
		std::signal(x.first, x.second.oldCallback);
	}
	signals.clear();
}

void Signals::staticOnSignal(int signum) {
	instance->onSignal(signum);
}

void Signals::onSignal(int signum) {
	auto i = signals.find(signum);
	assert(i != signals.end());
	// Copy is important
	auto cb = i->second.callback;
	cb();
}

} // namespace Utils
} // namespace Nov
