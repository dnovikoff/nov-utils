#ifndef NOV_UTILS_DETAIL_SINGLETON_DETAIL_HPP_
#define NOV_UTILS_DETAIL_SINGLETON_DETAIL_HPP_

#include <stdexcept>

namespace Nov {
namespace Utils {
namespace Detail {

class SingletonAssert {
public:
	static inline void onInit(void* pointer) {
		if (pointer == nullptr) {
			return;
		}
		throw std::logic_error("Singleton already initialized");
	}

	static inline void onGet(void* pointer) {
		if (pointer != nullptr) {
			return;
		}
		throw std::logic_error("Singleton not initialized");
	}
};

template<typename T>
class SingletonHolder {
	template<typename T1, typename T2>
	friend class SingletonScope;

	template<typename T1>
	friend class Singleton;

	inline static T* get() {
		return value;
	}

	inline static void set(T* v) {
		value = v;
	}

	static T* value;
};

template<typename T>
T* SingletonHolder<T>::value = nullptr;

} // namespace Detail
} // namespace Utils
} // namespace Nov

#endif // NOV_UTILS_DETAIL_SINGLETON_DETAIL_HPP_
