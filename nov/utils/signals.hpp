#ifndef NOV_UTILS_SIGNALS_HPP_
#define NOV_UTILS_SIGNALS_HPP_

#include <functional>
#include <map>

namespace Nov {
namespace Utils {

class Signals final {
public:
	typedef std::function<void()> Callback;

	Signals();
	~Signals();

	void installSignal(int x, const Callback& c);
	void installTerm(const Callback& c);
	void restoreSignals();

private:
	typedef void (*SingalFunction)(int);

	struct SignalInfo {
		Callback callback;
		SingalFunction oldCallback = nullptr;
	};

	void onSignal(int signum);
	static void staticOnSignal(int signum);

	static Signals* instance;
	std::map<int, SignalInfo> signals;
};

} // namespace Utils
} // namespace Nov

#endif // NOV_UTILS_SIGNALS_HPP_
