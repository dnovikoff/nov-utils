#ifndef NOV_UTILS_SINGLETON_HPP_
#define NOV_UTILS_SINGLETON_HPP_

#include <nov/utils/detail/singleton_detail.hpp>

namespace Nov {
namespace Utils {

template<typename T>
class Singleton {
public:
	Singleton(): instance_(Detail::SingletonHolder<T>::get()) {
		Detail::SingletonAssert::onGet(instance_);
	}
	inline T* operator->() {
		return instance_;
	}
	inline const T* operator->() const {
		return instance_;
	}

	inline T& operator*() {
		return *instance_;
	}
	inline const T& operator*() const {
		return *instance_;
	}

private:
	T* instance_ = nullptr;
};

} // namespace Utils
} // namespace Nov

#endif // NOV_UTILS_SINGLETON_HPP_
