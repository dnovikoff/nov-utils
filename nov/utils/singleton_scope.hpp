#ifndef NOV_UTILS_SINGLETON_SCOPE_HPP_
#define NOV_UTILS_SINGLETON_SCOPE_HPP_

#include <nov/utils/detail/singleton_detail.hpp>

namespace Nov {
namespace Utils {

template<typename T, typename RealType=T>
class SingletonScope {
public:
	template<typename... Args>
	explicit SingletonScope(Args&&... args): instance_(std::forward<Args>(args)...) {
		Detail::SingletonAssert::onInit(Detail::SingletonHolder<T>::get());
		Detail::SingletonHolder<T>::set(&instance_);
	}
	~SingletonScope() {
		Detail::SingletonHolder<T>::set(nullptr);
	}

	RealType& instance() {
		return instance_;
	}
	const RealType& instance() const {
		return instance_;
	}

private:
	RealType instance_;
};

} // namespace Utils
} // namespace Nov

#endif // NOV_UTILS_SINGLETON_SCOPE_HPP_
