#include "test.hpp"

#include <nov/utils/singleton.hpp>
#include <nov/utils/singleton_scope.hpp>

using Nov::Utils::Singleton;
using Nov::Utils::SingletonScope;

BOOST_AUTO_TEST_CASE (no_double_init) {
	SingletonScope<int> x;
	BOOST_CHECK_THROW((SingletonScope<int>()), std::logic_error);
}

BOOST_AUTO_TEST_CASE (not_inited) {
	BOOST_CHECK_THROW((Singleton<int>()), std::logic_error);
}

BOOST_AUTO_TEST_CASE (error_after_destruct) {
	{
		SingletonScope<int> x;
		BOOST_CHECK_NO_THROW((Singleton<int>()));
	}
	BOOST_CHECK_THROW((Singleton<int>()), std::logic_error);
}

BOOST_AUTO_TEST_CASE (singleton_value) {
	SingletonScope<int> x;
	Singleton<int> p;
	(*p) = 6;
	Singleton<int> p2;
	BOOST_CHECK_EQUAL((*p2), 6);
}

namespace {
struct A {
	virtual ~A() {
	}

	virtual int foo() = 0;
};

struct B: public A {
	virtual int foo() {
		return 42;
	}
};

struct C: public A {
	virtual int foo() {
		return 43;
	}
};

} // namespace

BOOST_AUTO_TEST_CASE (singleton_init_with_child_type) {
	SingletonScope<A, B> scope;
	Singleton<A> x1;
	BOOST_CHECK_EQUAL(x1->foo(), 42);
	// Still A initialized- not B
	BOOST_CHECK_THROW((Singleton<B>()), std::logic_error);

	// Already inited
	BOOST_CHECK_THROW((SingletonScope<A, C>()), std::logic_error);
}
